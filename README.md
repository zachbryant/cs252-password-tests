# cs252-password-tests

A tool for testing part1 of lab2

**Getting started**

Run the following commands inside your lab2-src folder:

~~~~
$ git submodule add https://gitlab.com/zachbryant/cs252-password-tests.git
$ git submodule update
$ cd cs252-password-tests
$ ln -s ../pwcheck.sh pwcheck.sh
$ ./pwtest.sh
~~~~

**Adding/removing cases**

Each line in testcases.txt corresponds to a score in testresults.txt. Be sure to keep everything in the right order.
