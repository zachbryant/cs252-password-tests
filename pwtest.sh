#!/bin/bash

c_echo(){
    RED="\033[0;31m"
    GREEN='\033[0;32m'
    YELLOW='\033[1;33m'
    NC='\033[0m' # No Color

    printf "${!1}%-3s\t%-32s\t%-24s\t%-24s${NC}" "${2}" "${3}" "${4}" "${5}"
}

TESTIN="testcases.txt"
TESTRES="testresults.txt"
PWCASES=()
while IFS="\n" read -r line; do PWCASES+=("$line"); done < $TESTIN
while IFS="\n" read -r line; do PWSCORE+=("$line"); done < $TESTRES

cd ..
NUMCASES=${#PWCASES[@]}
PASSED=0
printf "%-3s\t%-32s\t%-24s\t%-24s\n" "Len" "Password" "Score" "Expected"
printf "%-3s\t%-32s\t%-24s\t%-24s\n" "---" "----------" "----------" "----------"
for ((i=0; i < NUMCASES; i++)); do
	CASE="${PWCASES[i]}"
	EXPECTSCORE="${PWSCORE[i]}"
	echo "$CASE" > tmpcase.txt
	SCORE=$(./pwcheck.sh tmpcase.txt | egrep -o "\-?[0-9]+" || echo "Error: Password length invalid.")
	DIFF="$(diff -w --strip-trailing-cr <(echo -n "$SCORE") <(echo -n "$EXPECTSCORE"))"
	if [[ -z $DIFF ]]; then
		COLOR="GREEN"
		((PASSED++))
	else
		COLOR="RED"
	fi
	c_echo "$COLOR" "${#CASE}" "$CASE" "$SCORE" "$EXPECTSCORE"
	echo
done

echo
cd cs252-password-tests
RATIO=$(echo "$PASSED / $NUMCASES" | bc -l)
if (( $(echo "$RATIO == 1.0" | bc -l) )); then
	c_echo "GREEN" "All tests passed!" "" ""
elif (( $(echo "$RATIO > 0.5" | bc -l) )); then
	c_echo "YELLOW" "" "${PASSED} / ${NUMCASES} tests passed." "" ""
else
	c_echo "RED" "" "${PASSED} / ${NUMCASES} tests passed." "" ""
fi
echo
